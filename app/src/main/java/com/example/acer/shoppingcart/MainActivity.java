package com.example.acer.shoppingcart;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    String charles;
    TextView tv;
    ListView lv;
    ArrayAdapter<String> arrAdapter;
    ArrayList<String> arrList = new ArrayList<String>();
    double total = 0.0;
    int quantity;
    String message = "";
    Button btnSend;
    String number;
    String name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv = (TextView) findViewById(R.id.tv);
        lv = (ListView) findViewById(R.id.lv);
        arrAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,arrList);
        lv.setAdapter(arrAdapter);

        btnSend = (Button) findViewById(R.id.btnSend);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(arrList.size() == 0){
                    Toast.makeText(MainActivity.this, "Cannot send text! No items found.", Toast.LENGTH_SHORT).show();
                }
                else {
                    message += "Total: " + total;
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);
                    startActivityForResult(intent, 2);
                }
            }
        });
    }

    public void onClick(View v) {
        Intent intent = new Intent(MainActivity.this, QRScanner.class);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(data!=null) {
                charles = data.getStringExtra("cha");
                String[] split = charles.split("\\|\\|\\|");
                String temp = split[0] + ", " + split[1] + ", " + split[2];
                if (split.length != 3) {
                    Toast.makeText(MainActivity.this, "Invalid Code!", Toast.LENGTH_SHORT).show();
                } else if (split.length == 3) {
                    try {
                        quantity = Integer.parseInt(split[1]);
                        total += Double.parseDouble(split[2]) * quantity;
                        arrList.add(temp);
                        arrAdapter.notifyDataSetChanged();
                        message += temp + "\n";
                        tv.setText(total + "");
                    } catch (Exception e) {
                        Toast.makeText(MainActivity.this, "Invalid Code!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
            else{
                finishActivity(1);
            }
        }
        else if(requestCode == 2){
                if (data != null) {
                    Uri uri = data.getData();
                    if (uri != null) {
                        Cursor c = null;
                        try {
                            c = getContentResolver().query(uri, new String[]{
                                            ContactsContract.CommonDataKinds.Phone.NUMBER,
                                            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME},
                                    null, null, null);
                            if (c != null && c.moveToFirst()) {
                                number = c.getString(0);
                                name  = c.getString(1);
                            }
                        } finally {
                            if (c != null)
                                c.close();
                        }
                    }
                    SmsManager txt = SmsManager.getDefault();
                    txt.sendTextMessage(number,null,message,null,null);
                    Toast.makeText(MainActivity.this, "Sending to " + name + ".....", Toast.LENGTH_SHORT).show();
                }
            }
        else{
            finishActivity(2);
        }
        }
}